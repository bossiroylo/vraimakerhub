﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OxyLoca.DAL.Entities
{
    public class StatutMachine
    {
        public int IdStatutMachine { get; set; }
        public string Nom { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OxyLoca.DAL.Entities
{
    public class User
    {
        public int IdUser { get; set; }
        public int IdTypeUser { get; set; }
        public string Nom { get; set; }
        public string  Prenom { get; set; }
        public int Tel { get; set; }
        public string Email { get; set; }
        public string MotDePasse { get; set; }
    }
}

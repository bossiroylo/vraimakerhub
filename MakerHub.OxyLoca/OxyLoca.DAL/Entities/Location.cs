﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OxyLoca.DAL.Entities
{
    public class Location
    {
        public int IdLocation { get; set; }
        public int IdCommande { get; set; }
        public int IdMachine { get; set; }
        public string NomDuPatient { get; set; }
        public string PrenomDuPatient { get; set; }
        public string NumChambre { get; set; }
        public int RegistreNational { get; set; }
        public DateTime DebutLocation { get; set; }
        public DateTime FinLocation { get; set; }
    }
}

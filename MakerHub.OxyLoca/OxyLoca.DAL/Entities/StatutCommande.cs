﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OxyLoca.DAL.Entities
{
    public class StatutCommande
    {
        public int IdStatutCommande { get; set; }
        public string Nom { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OxyLoca.DAL.Entities
{
    public class TypePaiement
    {
        public int IdTypePaiement { get; set; }
        public string Nom { get; set; }
    }
}

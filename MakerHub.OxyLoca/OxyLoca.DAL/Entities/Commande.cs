﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OxyLoca.DAL.Entities
{
    public class Commande
    {
        public int IdCommande { get; set; }
        public int? IdStatutCommande { get; set; }
        public int? IdTypePaiement { get; set; }
        public int? IdUser { get; set; }
        public string Reference { get; set; }
        public float? Prix { get; set; }
        public int? Commantaire { get; set; }
    }
}

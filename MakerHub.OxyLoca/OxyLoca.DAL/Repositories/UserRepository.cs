﻿using OxyLoca.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Toolbox.ADO;

namespace OxyLoca.DAL.Repositories
{
    public class UserRepository : BaseRepo<User>
    {
        public User GetByCredential(string Email, string password) 
        {
            using (SqlConnection connect = GetConnection())
            {
                connect.Open();
                SqlCommand cmd = connect.CreateCommand();
                cmd.CommandText = "SELECT * FROM [USER] WHERE Email=@Email AND MotDePasse=@MotDePasse";
                    cmd.Parameters.AddWithValue("@Email", Email);
                    cmd.Parameters.AddWithValue("@MotDePasse", password);
                SqlDataReader r = cmd.ExecuteReader(); 
                if (r.Read())
                {
                    User result = new User();
                    foreach (var p in typeof(User).GetProperties())
                    {
                        p.SetValue(result, r[p.Name] == DBNull.Value ? null : r[p.Name]);
                    }
                    return result;
                }
            }
            return null;
        }


    }
}

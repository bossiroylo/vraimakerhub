﻿CREATE TABLE [dbo].[Location]
(
	[IdLocation] INT NOT NULL PRIMARY KEY IDENTITY,
	[IdCommande] INT NOT NULL,
	[IdMachine] INT NOT NULL,
	[NomDuPatient] NVARCHAR(150) NOT NULL,
	[PrenomDuPatient] NVARCHAR(150) NOT NULL,
	[NumChambre] VARCHAR(100) NOT NULL,
	[RegistreNational] VARCHAR(11) NOT NULL,
	[DebutLocation] DATE NOT NULL,
	[FinLocation] DATE NOT NULL,
	FOREIGN KEY (IdMachine) REFERENCES Machine,
	FOREIGN KEY (IdCommande) REFERENCES Commande,
)

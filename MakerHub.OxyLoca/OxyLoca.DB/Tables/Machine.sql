﻿CREATE TABLE [dbo].[Machine]
(
	[IdMachine] INT NOT NULL PRIMARY KEY IDENTITY,
	[IdUser] INT NULL,
	[IdStatutMachine] INT NULL,
	[NumeroSerie] INT NOT NULL,
	[Prix] INT NOT NULL,
	[Heure] TIME NULL,
	FOREIGN KEY (IdUser) REFERENCES [User],
	FOREIGN KEY (IdStatutMachine) REFERENCES StatutMachine,
)

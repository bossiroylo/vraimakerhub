﻿CREATE TABLE [dbo].[Commande]
(
	[IdCommande] INT NOT NULL PRIMARY KEY IDENTITY,
	[IdStatutCommande] INT NULL,
	[IdTypePaiement] INT NULL,
	[IdUser] INT NULL,
	[Reference] NVARCHAR(100) NOT NULL,
	[Date] DATETIME NOT NULL,
	[Prix] INT NULL,
	[Commentaire] NVARCHAR(254) NULL,
	FOREIGN KEY (IdStatutCommande) REFERENCES [StatutCommande],
	FOREIGN KEY (IdUser) REFERENCES [User],
	FOREIGN KEY (IdTypePaiement) REFERENCES [TypePaiement],
)

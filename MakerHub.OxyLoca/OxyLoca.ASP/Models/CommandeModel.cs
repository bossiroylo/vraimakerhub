﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OxyLoca.ASP.Models
{
    public class CommandeModel
    {
        public int IdCommande { get; set; }
        public int? IdStatutCommande { get; set; }
        public int? IdTypePaiement { get; set; }
        public int? IdUser { get; set; }
        public string Reference { get; set; }
        public float? Prix { get; set; }
        public int? Commantaire { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OxyLoca.ASP.Models
{
    public class UserModel
    {
        public int IdUser { get; set; }
        public int IdTypeUser { get; set; }
        public string Nom { get; set; }
        public string Prenom { get; set; }
        public int Tel { get; set; }
        public string Email { get; set; }
        public string MotDePasse { get; set; }
    }
}
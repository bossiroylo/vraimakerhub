﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OxyLoca.ASP.Models
{
    public class LoginModel
    {
        public string Email { get; set; }
        public string MotDePasse { get; set; }
    }
}
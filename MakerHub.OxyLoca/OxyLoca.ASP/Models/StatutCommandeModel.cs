﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OxyLoca.ASP.Models
{
    public class StatutCommandeModel
    {
        public int IdStatutCommande { get; set; }
        public string Nom { get; set; }
    }
}
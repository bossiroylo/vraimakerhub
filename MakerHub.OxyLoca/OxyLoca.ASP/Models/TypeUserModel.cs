﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OxyLoca.ASP.Models
{
    public class TypeUserModel
    {
        public int IdTypeUser { get; set; }
        public string Nom { get; set; }
    }
}
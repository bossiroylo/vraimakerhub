﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OxyLoca.ASP.Models
{
    public class MachineModel
    {
        public int IdMachine { get; set; }
        public int? IdUser { get; set; }
        public int? IdStatutMachine { get; set; }
        public int NumeroSerie { get; set; }
        public float Prix { get; set; }
        public TimeSpan? Heure { get; set; }
    }
}
﻿using OxyLoca.ASP.Mappers;
using OxyLoca.ASP.Models;
using OxyLoca.DAL.Entities;
using OxyLoca.DAL.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace OxyLoca.ASP.Controllers
{
    public class LocationController : ApiController
    {
        public IEnumerable<LocationModel> GetAll()
        {
            LocationRepository repo = new LocationRepository();
            return repo.GetAll().Select(x => x.Mapto<LocationModel>());
        }
        public LocationModel Get(int id)
        {
            LocationRepository repo = new LocationRepository();
            return repo.GetById(id)?.Mapto<LocationModel>();
        }

        public void Delete(int id)
        {
            LocationRepository repo = new LocationRepository();
            repo.Delete(id);

        }

        public int Post(LocationModel model)
        {
            LocationRepository repo = new LocationRepository();
            return repo.Insert(model.Mapto<Location>());
        }

        public void Put(LocationModel model)
        {
            LocationRepository repo = new LocationRepository();
            repo.update(model.Mapto<Location>());
        }
    }
}

﻿using OxyLoca.ASP.Mappers;
using OxyLoca.ASP.Models;
using OxyLoca.DAL.Entities;
using OxyLoca.DAL.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace OxyLoca.ASP.Controllers
{
    public class CommandeController : ApiController
    {
        public IEnumerable<CommandeModel> GetAll()
        {
            CommandeRepository repo = new CommandeRepository();
            return repo.GetAll().Select(x => x.Mapto<CommandeModel>());
        }
        public CommandeModel Get(int id)
        {
            CommandeRepository repo = new CommandeRepository();
            return repo.GetById(id)?.Mapto<CommandeModel>();
        }

        public void Delete(int id)
        {
            CommandeRepository repo = new CommandeRepository();
            repo.Delete(id);

        }

        public int Post(CommandeModel model)
        {
            CommandeRepository repo = new CommandeRepository();
            return repo.Insert(model.Mapto<Commande>());
        }

        public void Put(CommandeModel model)
        {
            CommandeRepository repo = new CommandeRepository();
            repo.update(model.Mapto<Commande>());
        }
    }
}

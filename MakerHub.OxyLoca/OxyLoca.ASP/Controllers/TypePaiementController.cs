﻿using OxyLoca.ASP.Mappers;
using OxyLoca.ASP.Models;
using OxyLoca.DAL.Entities;
using OxyLoca.DAL.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace OxyLoca.ASP.Controllers
{
    public class TypePaiementController : ApiController
    {
            public IEnumerable<TypePaiementModel> GetAll()
            {
                TypePaiementRepository repo = new TypePaiementRepository();
                return repo.GetAll().Select(x => x.Mapto<TypePaiementModel>());
            }
            public TypePaiementModel Get(int id)
            {
                TypePaiementRepository repo = new TypePaiementRepository();
                return repo.GetById(id)?.Mapto<TypePaiementModel>();
            }

            public void Delete(int id)
            {
                TypePaiementRepository repo = new TypePaiementRepository();
                repo.Delete(id);

            }

            public int Post(TypePaiementModel model)
            {
                TypePaiementRepository repo = new TypePaiementRepository();
                return repo.Insert(model.Mapto<TypePaiement>());
            }

            public void Put(TypePaiementModel model)
            {
                TypePaiementRepository repo = new TypePaiementRepository();
                repo.update(model.Mapto<TypePaiement>());
            }
        
    }
}

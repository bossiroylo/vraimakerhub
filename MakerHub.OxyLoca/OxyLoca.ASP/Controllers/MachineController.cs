﻿using OxyLoca.ASP.Mappers;
using OxyLoca.ASP.Models;
using OxyLoca.DAL.Entities;
using OxyLoca.DAL.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace OxyLoca.ASP.Controllers
{
    public class MachineController : ApiController
    {
        public IEnumerable<MachineModel> GetAll()
        {
            MachineRepository repo = new MachineRepository();
            return repo.GetAll().Select(x => x.Mapto<MachineModel>());
        }
        public MachineModel Get(int id)
        {
            MachineRepository repo = new MachineRepository();
            return repo.GetById(id)?.Mapto<MachineModel>();
        }

        public void Delete(int id)
        {
            MachineRepository repo = new MachineRepository();
            repo.Delete(id);

        }

        public int Post(MachineModel model)
        {
            MachineRepository repo = new MachineRepository();
            return repo.Insert(model.Mapto<Machine>());
        }

        public void Put(MachineModel model)
        {
            MachineRepository repo = new MachineRepository();
            repo.update(model.Mapto<Machine>());
        }
    }
}

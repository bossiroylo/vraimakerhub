﻿using OxyLoca.ASP.Mappers;
using OxyLoca.ASP.Models;
using OxyLoca.DAL.Entities;
using OxyLoca.DAL.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace OxyLoca.ASP.Controllers
{
    public class UserController : ApiController
    {
        public IEnumerable<UserModel> GetAll()
        {
            UserRepository repo = new UserRepository();
            return repo.GetAll().Select(x => x.Mapto<UserModel>());
        }
        public UserModel Get(int id)
        {
            UserRepository repo = new UserRepository();
            return repo.GetById(id)?.Mapto<UserModel>();
        }

        public void Delete(int id)
        {
            UserRepository repo = new UserRepository();
            repo.Delete(id);

        }

        public int Post(UserModel model)
        {
            UserRepository repo = new UserRepository();
            return repo.Insert(model.Mapto<User>());
        }

        public void Put(UserModel model)
        {
            UserRepository repo = new UserRepository();
            repo.update(model.Mapto<User>());
        }

        [HttpPost]
        [Route ("api/login")]
        public UserModel Login(LoginModel model) 
        {

            UserRepository repo = new UserRepository();
            return repo.GetByCredential(model.Email, model.MotDePasse)?.Mapto<UserModel>();
        }
    }
}

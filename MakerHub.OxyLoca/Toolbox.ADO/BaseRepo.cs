﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Toolbox.ADO
{
    public class BaseRepo<T>
        where T : new()
    {
        private string ConnectionString;
        public BaseRepo()
        {
            ConnectionString = ConfigurationManager.ConnectionStrings["default"].ConnectionString;
        }

        protected SqlConnection GetConnection()
        {
            return new SqlConnection(ConnectionString);
        }

        protected string GetTableName()
        {
            return typeof(T).Name;
        }


        public IEnumerable<T> GetAll()
        {
            using (SqlConnection connect = GetConnection())
            {
                connect.Open();
                SqlCommand cmd = connect.CreateCommand();
                cmd.CommandText = "SELECT * FROM [" + GetTableName() +"]";
                SqlDataReader r = cmd.ExecuteReader();
                while (r.Read())
                {
                    T result = new T();
                    foreach (var p in typeof(T).GetProperties())
                    {
                        p.SetValue(result, r[p.Name] == DBNull.Value ? null : r[p.Name]);
                    }
                    yield return result;
                }
            };
        }

        public T GetById(int id)
        {
            using (SqlConnection connect = GetConnection())
            {
                connect.Open();
                SqlCommand cmd = connect.CreateCommand();
                cmd.CommandText = "SELECT * FROM [" + GetTableName() + "] WHERE Id" + GetTableName() + "= @id";
                cmd.Parameters.AddWithValue("@id", id);
                SqlDataReader r = cmd.ExecuteReader();
                if (r.Read())
                {
                    T result = new T();
                    foreach (var p in typeof(T).GetProperties())
                    {
                        p.SetValue(result, r[p.Name] == DBNull.Value ? null : r[p.Name]);
                    }
                    return result;
                }
                return default(T);

            };
        }

        public virtual int Insert(T entity)
        {
            using (SqlConnection connect = GetConnection())
            {
                connect.Open();
                SqlCommand cmd = connect.CreateCommand();
                cmd.CommandText = "INSERT INTO [" + GetTableName() + "] (";
                var props = typeof(T).GetProperties().Where(p => p.Name != "Id" + GetTableName());
                string s1 = string.Join(",", props.Select(p => "[" + p.Name + "]"));
                cmd.CommandText += s1 + ") OUTPUT INSERTED.Id" + GetTableName() + " VALUES (" + string.Join(",", props.Select(p => "@" + p.Name)) + ")";
                foreach (var item in props)
                {
                    cmd.Parameters.AddWithValue("@" + item.Name, item.GetValue(entity) ?? DBNull.Value);
                }
                return (int)cmd.ExecuteScalar();
            }
        }

        public virtual bool Delete(int id)
        {
            using (SqlConnection connect = GetConnection())
            {
                connect.Open();
                SqlCommand cmd = connect.CreateCommand();
                cmd.CommandText = "DELETE FROM [" + GetTableName() + "] WHERE Id" + GetTableName() + "=@id";
                cmd.Parameters.AddWithValue("@id", id);
                return cmd.ExecuteNonQuery() == 1;
            }
        }

        public virtual bool update(T entity)
        {
            using (SqlConnection connect = GetConnection())
            {
                connect.Open();
                SqlCommand cmd = connect.CreateCommand();
                cmd.CommandText = "UPDATE [" + GetTableName() + "] SET ";
                var props = typeof(T).GetProperties().Where(p => p.Name != "Id" + GetTableName());
                string s1 = string.Join(",", props.Select(p => p.Name + "=" + "@" + p.Name));
                cmd.CommandText += s1 + " WHERE Id" + GetTableName() + "=@" + "Id" + GetTableName();

                foreach (var item in typeof(T).GetProperties())
                {
                    cmd.Parameters.AddWithValue("@" + item.Name, item.GetValue(entity) ?? DBNull.Value);
                }

                return cmd.ExecuteNonQuery() == 1;
            }
        }
    }   
}
